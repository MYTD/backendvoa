@extends('layouts.default')
@section('title')
    Data Tables | Clear Admin Template
@stop
@section('styles')
   <!--page level css -->
   <link rel="stylesheet" type="text/css" href="vendors/datatables/css/dataTables.bootstrap4.css"/>
   <link rel="stylesheet" type="text/css" href="css/custom_css/datatables_custom.css">
   <!--end of page level css-->
@stop
@section('content')
   <!-- /.right-side -->
   <aside class="right-side">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h1 >
               จัดการ Image หน้าแรก
         </h1>
      </section>
      <!-- Main content -->
      <section class="content">
         <div class="row">
            <div class="col-lg-12">
               <div class="card ">
                  <div class="card-header">
                        <h3 class="card-title">
                           <i class="ti-"></i> รายการ รูปภาพ
                        </h3>
                        <span class="float-right">
                           <i class="fa fa-fw ti-angle-up clickable"></i>
                           <i class="fa fa-fw ti-close removecard"></i>
                        </span>
                  </div>
                  <div class="card-body">
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover" id="sample_1"  >
                              <thead>
                                 <tr>
                                    <th>
                                       Name
                                    </th>
                                    <th>email</th>
                                    <th>
                                       Phone
                                    </th>
                                    <th>
                                       Department
                                    </th>
                                    <th>Salary</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Roman Runolfsdottir</td>
                                    <td>Roman24@yahoo.com</td>
                                    <td>429-509-9163</td>
                                    <td>Toys</td>
                                    <td>49428</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- First Basic Table Ends Here-->
         <!-- Second Data Table Starts Here-->

         <!--rightside bar -->

      </section>
      <!-- /.content -->
   </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="vendors/datatables/js/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="js/custom_js/datatables_custom.js"></script>
    <!-- end of page level js -->
@stop

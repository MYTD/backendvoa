@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Service And Work
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> เพิ่มข้อมูล
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('serviceandwork')}}" method="post" enctype="multipart/form-data">
                              {{method_field('POST')}}
                             {{ csrf_field() }}

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">Upload Image (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" required name='img'/>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="row">

                                        {{-- Range --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Range</h5>
                                          <input type="number" class="form-control" placeholder="ลำดับการแสดงผล" name='range' required/>
                                        </div>

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary m-t-10">
                                            Submit
                                          </button>
                                          <a href="{{url('home')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->
@stop

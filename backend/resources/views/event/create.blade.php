@extends('layouts.default')
@section('title')
    Form Editors | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link href="{{asset('vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('vendors/trumbowyg/ui/trumbowyg.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="screen" type="text/css" href="{{asset('css/summernote.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/form_editors.css')}}">
    <!--end of page level css-->

    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                จัดการข้อความ
            </h1>

        </section>
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <!-- /.box -->

            <div class="card mrgn_top">
                <div class="card-header">
                    <div class=" bootstrap-admin-box-title ">
                        <h3 class="card-title">Text</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="bootstrap-admin-card-content summer_noted">
                      <form class="" action="{{url('event')}}" method="post" enctype='multipart/form-data'>
                        {{csrf_field()}}
                        {{method_field('POST')}}

                        <div class="row">
                          <div class="col-md-12">
                              <h5 class="h5pnl_font">Upload Image (File Type: PNG, JPG )</h5>
                              <input type="file" class="dropify" data-allowed-file-extensions="png jpg" required name='img' data-default-file='{{asset('/backend/storage/app/logouploadimage/370x246.png')}}'/>
                          </div>

                          <div class="col-lg-12" style="margin-top: 3%">
                            <div class="form-group">
                              <label for="">หัวข้อ</label>
                              <input type="text" class="form-control" name="texttitle" placeholder="">
                            </div>
                          </div>

                          <div class="col-lg-12" style="margin-top:3%">
                            <textarea name="detail" id='summernote'></textarea>
                          </div>

                          <div class="col-lg-12">
                            {{-- New Row --}}
                            <div class="row">
                              {{-- col 4 --}}
                              <div class="col-lg-4">
                                {{-- group input --}}
                                <div class="form-group">
                                  <label for="">Event Date</label>
                                  <input type="text" class="form-control" name="eventdate" placeholder="Ex: 11 July 2018">
                                </div>
                                {{-- End group input --}}
                              </div>
                              {{-- End col 4 --}}
                              {{-- col 4 --}}
                              <div class="col-lg-4">
                                {{-- group input --}}
                                <div class="form-group">
                                  <label for="">Event Time</label>
                                  <input type="text" class="form-control" name="eventtime" placeholder="Ex: 20:00pm - 22:00pm">
                                </div>
                                {{-- End group input --}}
                              </div>
                              {{-- End col 4 --}}
                              {{-- col 4 --}}
                              <div class="col-lg-4">
                                {{-- group input --}}
                                <div class="form-group">
                                  <label for="">Event Location</label>
                                  <input type="text" class="form-control" name="eventlocation" placeholder="Khon Kaen" value="Khon Kaen">
                                </div>
                                {{-- End group input --}}
                              </div>
                              {{-- End col 4 --}}
                            </div>
                            {{-- End Row --}}
                          </div>
                        </div>

                        <div class="col-lg-12">
                          <div class="form-group">
                            <label for="">รูปอื่นๆ</label>
                            <input type="file" id="upload_file" name="imgother[]" onchange="preview_image();" multiple/>
                          </div>
                        </div>


                        <div class="col-lg-12 text-right">
                          <button type="submit" class="btn btn-primary m-t-10">Save</button>
                          <a href="{{url('event')}}" class="btn btn-danger m-t-10">Cancel</a>
                        </div>
                      </form>


                      <div class="row" id='image_preview'></div>
                    </div>
                </div>
            </div>

            <!--rightside bar -->

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/trumbowyg/js/trumbowyg.js')}}" type="text/javascript"></script>
    <!--<script src="vendors/trumbowyg/ui/icons.svg" type="text/javascript"></script>-->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom_js/form_editors.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->


    <script>


  function preview_image() {
      $(".jobimg").remove()
      var total_file=document.getElementById("upload_file").files.length;
      for(var i=0;i<total_file;i++) {
        $('#image_preview').append("<div class='jobimg col-lg-6'> &nbsp; <img src='"+URL.createObjectURL(event.target.files[i])+"' style='max-width:100%;'></div>");
      }
    }
  </script>
@stop

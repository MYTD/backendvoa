@extends('layouts.default')
@section('title')
    Form Editors | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link href="{{asset('vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('vendors/trumbowyg/ui/trumbowyg.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="screen" type="text/css" href="{{asset('css/summernote.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/form_editors.css')}}">
    <!--end of page level css-->
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                จัดการข้อความ
            </h1>

        </section>
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <!-- /.box -->

            <div class="card mrgn_top">
                <div class="card-header">
                    <div class=" bootstrap-admin-box-title ">
                        <h3 class="card-title">
                            Text</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="bootstrap-admin-card-content summer_noted">
                      <form class="" action="{{url('hometext/'.$resources->id)}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <textarea name="text" id='summernote'>{!! $resources->text !!}</textarea>


                        <div class="col-lg-12 text-right">
                          <button type="submit" class="btn btn-primary m-t-10">Save</button>
                          <a href="{{url('hometext')}}" class="btn btn-danger m-t-10">Cancel</a>

                        </div>
                      </form>

                    </div>
                </div>
            </div>

            <!--rightside bar -->

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/trumbowyg/js/trumbowyg.js')}}" type="text/javascript"></script>
    <!--<script src="vendors/trumbowyg/ui/icons.svg" type="text/javascript"></script>-->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom_js/form_editors.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->
@stop

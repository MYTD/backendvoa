@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->


    <!--page level css -->
    <link href="{{asset('vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('vendors/trumbowyg/ui/trumbowyg.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="screen" type="text/css" href="{{asset('css/summernote.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/form_editors.css')}}">
    <!--end of page level css-->
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dropify
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#"> Gallery</a>
                </li>
                <li class="active">
                    Dropify
                </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> Dropify
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('aboutus')}}" method="post" enctype="multipart/form-data">
                              {{method_field('POST')}}
                             {{ csrf_field() }}

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">Upload Image (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" required name='img'/>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="row">

                                        {{-- Range --}}
                                        <div class="col-md-12">
                                          <textarea name="text" id='summernote'></textarea>
                                        </div>

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary m-t-10">
                                            Submit
                                          </button>
                                          <a href="{{url('home')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/trumbowyg/js/trumbowyg.js')}}" type="text/javascript"></script>
    <!--<script src="vendors/trumbowyg/ui/icons.svg" type="text/javascript"></script>-->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom_js/form_editors.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->
@stop

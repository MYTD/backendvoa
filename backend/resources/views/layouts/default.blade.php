<!DOCTYPE html>
{{-- menu# --}}
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}">
    <!-- end of global css -->
    @yield('styles')
</head>
<body class="skin-default">
<div class="preloader">
    <div class="loader_img"><img src="{{asset('img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="index" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the marginin -->
            <img src="{{asset('img/logo_white.png')}}" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div class="mr-auto">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                        class="fa fa-fw ti-menu"></i>
            </a>
        </div>

    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="float-left profile-thumb" href="#">
                            <img src="{{asset('img/authors/avatar1.jpg')}}" class="rounded-circle" alt="User Image"></a>
                        <div class="content-profile">
                            <h4 class="media-heading">CMS</h4>
                        </div>
                    </div>
                </div>
                <ul class="navigation">

                  {{-- user --}}
                  <li {!! ((Request::is('user') || Request::is('user/*')) ? 'class="active"' : '') !!} id="active">
                      <a href="{{url('user')}}">
                          <i class="fa fa-fw fa-sun-o"></i>
                          <span class="mm-text ">User</span>
                      </a>
                  </li>
                  {{-- End user --}}


                    {{-- Home --}}
                    <li class="menu-dropdown {!! Request::is('home')||Request::is('home/*')|| Request::is('hometext') || Request::is('hometext/*')  ? 'active' : '' !!}">
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-home"></i><span>Homes</span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="sub-menu">
                            <li {!! (Request::is('home') ? 'class="active"' : 'class=""') !!}>
                                <a href="{{url('home')}}">
                                    <i class="fa fa-fw fa-minus"></i> Slite
                                </a>
                            </li>
                            <li {!! ((Request::is('hometext') || Request::is('hometext/*')) ? 'class="active"' : 'class=""') !!}>
                                <a href="{{url('hometext')}}">
                                    <i class="fa fa-fw fa-minus"></i> Text
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- END Home --}}


                    {{-- aboutus --}}
                    <li {!! ((Request::is('aboutus') || Request::is('aboutus/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('aboutus')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">About Us</span>
                        </a>
                    </li>
                    {{-- End aboutus --}}




                    {{-- Service And Work --}}
                    <li {!! ((Request::is('serviceandwork') || Request::is('serviceandwork/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('serviceandwork')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Service And Work</span>
                        </a>
                    </li>
                    {{-- End Service And Work --}}




                    {{-- Products --}}
                    <li {!! ((Request::is('product') || Request::is('product/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('product')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Products</span>
                        </a>
                    </li>
                    {{-- End Products --}}




                    {{-- creator --}}
                    <li {!! ((Request::is('creator') || Request::is('creator/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('creator')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Creators</span>
                        </a>
                    </li>
                    {{-- End cretor --}}




                    {{-- event --}}
                    <li {!! ((Request::is('event') || Request::is('event/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('event')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Events</span>
                        </a>
                    </li>
                    {{-- End event --}}




                    {{-- Aesthetics Of Living --}}
                    <li {!! ((Request::is('AOL') || Request::is('AOL/*')) ? 'class="active"' : '') !!} id="active">
                        <a href="{{url('AOL')}}">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Aesthetics Of Living</span>
                        </a>
                    </li>
                    {{-- End Aesthetics Of Living --}}





                    {{-- Aesthetics Of Living --}}
                    <li id="active">
                      <form class="" action="{{url('logout')}}" method="post" id='logout'>
                        {{ csrf_field() }}
                        {{method_field('POST')}}
                      </form>
                        <a href="javascript:;" onclick="document.getElementById('logout').submit();">
                            <i class="fa fa-fw fa-sun-o"></i>
                            <span class="mm-text ">Logout</span>
                        </a>
                    </li>
                    {{-- End Aesthetics Of Living --}}





                </ul>
                <!-- / .navigation --> </div>
            <!-- menu --> </section>
        <!-- /.sidebar -->
    </aside>
    @yield('content')
</div>
@yield('tabs_accordions')
<!-- global js -->
<div id="qn"></div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<!-- end of global js -->
@yield('scripts')
</body>
</html>

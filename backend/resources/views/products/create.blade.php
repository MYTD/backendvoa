@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dropify
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#"> Gallery</a>
                </li>
                <li class="active">
                    Dropify
                </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> Dropify
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('product')}}" method="post" enctype="multipart/form-data">
                              {{method_field('POST')}}
                             {{ csrf_field() }}

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">Upload Image (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" required name='img'/>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="row">

                                        {{-- Url --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">ตำแหน่งในการแสดง</h5>
                                          <select class="form-control" name="type">
                                            <option value="top">แถวบน</option>
                                            <option value="below">แถวล่าง</option>
                                          </select>
                                        </div>

                                        {{-- Url --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">URL</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='url' autocomplete="off"/>
                                        </div>

                                        {{-- Width --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Width</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='w' autocomplete="off" value="170"/>
                                        </div>

                                        {{-- Height --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Height</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='h' autocomplete="off" value="170"/>
                                        </div>

                                        {{-- Range --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Range</h5>
                                          <input type="number" class="form-control" placeholder="ลำดับการแสดงผล" name='range' required/>
                                        </div>

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary m-t-10">
                                            Submit
                                          </button>
                                          <a href="{{url('product')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->
@stop

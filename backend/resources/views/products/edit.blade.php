@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Product
            </h1>

        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> Update Product
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('product/'.$resources->id)}}" method="post" enctype="multipart/form-data">
                              {{method_field('PATCH')}}
                             {{ csrf_field() }}

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">Upload Image (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" name='img'  data-default-file="{{asset($resources->img)}}"/>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="row">

                                        {{-- Url --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">ตำแหน่งในการแสดง</h5>
                                          <select class="form-control" name="type">
                                            <option value="top" {{ $resources->type == 'top' ? 'selected' : ''}}>แถวบน</option>
                                            <option value="below" {{ $resources->type == 'below' ? 'selected' : ''}}>แถวล่าง</option>
                                          </select>
                                        </div>

                                        {{-- Url --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">URL</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='url' autocomplete="off" value="{{$resources->link}}"/>
                                        </div>

                                        {{-- Width --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Width</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='w' autocomplete="off" value="{{$resources->w}}"/>
                                        </div>

                                        {{-- Height --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Height</h5>
                                          <input type="text" class="form-control" placeholder="ลำดับการแสดงผล" name='h' autocomplete="off" value="{{$resources->h}}"/>
                                        </div>

                                        {{-- Range --}}
                                        <div class="col-md-12">
                                          <h5 class="h5pnl_font">Range</h5>
                                          <input type="number" class="form-control" placeholder="ลำดับการแสดงผล" name='range' required value="{{$resources->range}}"/>
                                        </div>

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary m-t-10">
                                            Submit
                                          </button>
                                          <a href="{{url('product')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->
@stop

@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('')}}/vendors/datatables/css/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('')}}/css/custom_css/datatables_custom.css">
    <!--end of page level css-->

    <link rel="stylesheet" type="text/css" href="{{asset('')}}/vendors/fancybox/jquery.fancybox.css" media="screen"/>
    <link href="{{asset('')}}/css/animated-masonry-gallery.css" rel="stylesheet" type="text/css"/>


    <!--page level css -->
    <link href="{{asset('')}}/vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('')}}/css/buttons.min.css">
    <link rel="stylesheet" href="{{asset('')}}/vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link rel="stylesheet" href="{{asset('')}}/vendors/laddabootstrap/css/ladda.min.css">
    <link rel="stylesheet" href="{{asset('')}}/vendors/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css">
    <link href="{{asset('')}}/css/buttons_sass.css" rel="stylesheet">
    <link href="{{asset('')}}/css/advbuttons.css" rel="stylesheet">
    <!--end of page level css-->

@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dropify
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#"> Gallery</a>
                </li>
                <li class="active">
                    Dropify
                </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> Dropify
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('creator/'.$resources->id)}}" method="post" enctype="multipart/form-data">
                              {{method_field('PATCH')}}
                               {{ csrf_field() }}

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">รู้เจ้าของผลงาน (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" name='img' data-default-file='{{asset($resources->image)}}'/>
                                  </div>
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">ผลงาน (File Type: PNG, JPG )</h5>
                                      {{-- <input type="file" class="dropify" onchange="preview_image();" multiple required name='imagejob[]'/> --}}

                                      <input type="file" class="dropify"  name='imagejob[]' onchange="preview_image();" id='upload_file' multiple/>
                                  </div>

                                  <div class="col-md-12">
                                      <div class="row">

                                        {{-- Range --}}
                                        {{-- <div class="col-md-6">
                                          <h5 class="h5pnl_font">Range</h5>
                                          <input type="number" class="form-control" placeholder="ลำดับการแสดงผล" name='range' value="{{$resources->range}}"/>
                                        </div> --}}

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right" style="margin-top:3%">
                                          <button type="submit" class="btn btn-primary m-t-10" >
                                            Submit
                                          </button>
                                          <a href="{{url('creator')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                            <h5>
                              Image Preview
                            </h5>

                            <div class="row">
                              @foreach ($resources->creatorjob as $row)
                                <div class="col-lg-4">
                                  <div class="col-lg-12 text-right">

                                    <form class="" action="{{url('creator/imagejob/'.$row->id)}}" method="post">
                                      {{method_field('DELETE')}}
                                       {{ csrf_field() }}
                                       <button type="submit" class="btn btn-sm btn-danger">
                                         X
                                       </button>
                                    </form>

                                  </div>
                                  <a class="fancybox img-fluid" href="{{asset($row->listimg)}}"
                                     data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                                      <img alt="gallery" src="{{asset($row->listimg)}}" class="all studio" style="width:100%; margin-top:3%"/>
                                  </a>

                                  {{-- <input type="text" name="" value="{{$row->text}}" class="form-control"> --}}

                                </div>
                              @endforeach



                              <div class="col-lg-4" >
                                <div class="row" id='image_preview'>

                                  {{-- <img id="blah" alt="imgshowing" class="col-lg-3 jobimg" style="width:485; height:325; margin-top:10px" /> --}}
                                </div>
                              </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendors/datatables/js/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/datatables_custom.js')}}"></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script src="{{asset('')}}/js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="{{asset('')}}/vendors/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="{{asset('')}}/vendors/fancybox/helpers/jquery.fancybox-buttons.js" type="text/javascript"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="{{asset('')}}/vendors/fancybox/jquery.fancybox.js"></script>
    <script src="{{asset('')}}/js/animated-masonry-gallery.js" type="text/javascript"></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('')}}/js/buttons.js"></script>
    <script type="text/javascript" src="{{asset('')}}/vendors/laddabootstrap/js/spin.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}/vendors/laddabootstrap/js/ladda.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}/vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script type="text/javascript" src="{{asset('')}}/js/custom_js/button_main.js"></script>
    <!-- end of page level js -->

    <script>
    function preview_image() {
        $(".jobimg").remove()
        var total_file=document.getElementById("upload_file").files.length;
        for(var i=0;i<total_file;i++)
        {
          $('#image_preview').append("<div class='jobimg col-lg-12'><img src='"+URL.createObjectURL(event.target.files[i])+"' style='max-width:100%;margin-top:10px;'><div class='form-group'><label>ข้อความ</label><input type='text' class='form-control col-lg-10' name='text[] hidden'></div></div>");
        }
      }
    </script>
@stop

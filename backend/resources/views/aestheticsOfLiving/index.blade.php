@extends('layouts.default')
@section('title')
    Data Tables | Clear Admin Template
@stop
@section('styles')
   <!--page level css -->
   <link rel="stylesheet" type="text/css" href="vendors/datatables/css/dataTables.bootstrap4.css"/>
   <link rel="stylesheet" type="text/css" href="css/custom_css/datatables_custom.css">
   <!--end of page level css-->

   <link rel="stylesheet" type="text/css" href="vendors/fancybox/jquery.fancybox.css" media="screen"/>
   <link href="css/animated-masonry-gallery.css" rel="stylesheet" type="text/css"/>


   <!--page level css -->
   <link href="vendors/hover/css/hover-min.css" rel="stylesheet">
   <link rel="stylesheet" href="css/buttons.min.css">
   <link rel="stylesheet" href="vendors/laddabootstrap/css/ladda-themeless.min.css">
   <link rel="stylesheet" href="vendors/laddabootstrap/css/ladda.min.css">
   <link rel="stylesheet" href="vendors/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css">
   <link href="css/buttons_sass.css" rel="stylesheet">
   <link href="css/advbuttons.css" rel="stylesheet">
   <!--end of page level css-->
@stop
@section('content')
   <!-- /.right-side -->
   <aside class="right-side">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h1>
             Aesthetics Of Living
         </h1>
      </section>
      <!-- Main content -->
      <section class="content">



         <div class="row">
           <div class="col-lg-12">
               <div class="card ">
                   <div class="card-header">
                       <h3 class="card-title">
                           <i class="ti-layout-tab-window"></i> Aesthetics Of Living
                       </h3>
                       <span class="float-right">
                               <i class="fa fa-fw ti-angle-up clickable"></i>
                               <i class="fa fa-fw ti-close removecard"></i>
                       </span>
                   </div>
                   <div class="card-body">
                       <div class="bs-example">
                           <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                               <li class="nav-item">
                                   <a href="#Exterior" data-toggle="tab" class="nav-link active">Exterior</a>
                               </li>
                               <li class="nav-item">
                                   <a href="#Interior" data-toggle="tab" class="nav-link">Interior</a>
                               </li>
                               <li class="nav-item">
                                   <a href="#Landscape" data-toggle="tab" class="nav-link">Landscape</a>
                               </li>


                           </ul>
                           <div id="myTabContent" class="tab-content">
                               <div class="tab-pane active" id="Exterior">
                                 <div class="col-lg-12 text-right">
                                   <a href="{{url('AOL/Exterior')}}" class="button button-pill button-action-flat hvr-pulse">
                                     เพิ่มข้อมูล Exterior
                                   </a>
                                 </div>

                                 <table class="table table-striped table-bordered table-hover" id="sample_1" style="margin-top:1.5%" >
                                   <thead>
                                     <th class="text-center">รูป</th>
                                     <th class="text-center">ข้อความ</th>
                                     <th class="text-center">Type</th>
                                     <th class="text-center"></th>
                                   </thead>
                                   <tbody>
                                     @foreach (($datas['Exterior']) as $row)
                                       <tr>
                                         <td width='30%'>
                                           <a class="fancybox img-fluid" href="{{asset($row->imageshowing)}}"
                                              data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                                               <img alt="gallery" src="{{asset($row->imageshowing)}}" class="all studio" style="width:100%"/>
                                           </a>
                                         </td>

                                         <td>
                                           {{
                                             str_limit(strip_tags($row->text) , 200, '...')
                                           }}
                                         </td>
                                         <td>
                                           {{
                                             $row->imgpopup ? 'Popup' : 'Main'
                                           }}
                                         </td>
                                         <td>
                                           <div class="btn-group drop_btn" role="group">
                                               <button type="button" class="button button-pill button-royal-flat hvr-push "
                                                       id="exampleIconDropdown1" data-toggle="dropdown"
                                                       aria-expanded="false">
                                                   Action
                                                   <span class="caret"></span>
                                               </button>
                                               <ul class="dropdown-menu dropdown_position1" aria-labelledby="exampleIconDropdown1"
                                                   role="menu">
                                                   <li role="presentation">
                                                     <a href="{{url('AOL/'.$row->id.'/edit')}}" role="menuitem">Edit</a></li>
                                                   <li role="presentation">
                                                     <form action="{{url('AOL/'.$row->id)}}" method="post" name='form{{$row->id}}'>
                                                       {{method_field('DELETE')}}
                                                       {{ csrf_field() }}
                                                     </form>
                                                     <a href="javascript:void(0)" onclick="document.forms['form{{$row->id}}'].submit(); return false;"
                                                                              role="menuitem">Delete</a>
                                                     </li>
                                               </ul>
                                           </div>
                                         </td>
                                       </tr>
                                     @endforeach
                                   </tbody>
                                 </table>

                               </div>


                               <div class="tab-pane fade" id="Landscape">
                                 <div class="col-lg-12 text-right">
                                   <a href="{{url('AOL/Landscape')}}" class="button button-pill button-action-flat hvr-pulse">
                                     เพิ่มข้อมูล Landscape
                                   </a>
                                 </div>

                                 <table class="table table-striped table-bordered table-hover" id="example" style="margin-top:1.5%" >
                                   <thead>
                                     <th class="text-center">รูป</th>
                                     <th class="text-center">ข้อความ</th>
                                     <th class="text-center">Type</th>
                                     <th class="text-center"></th>
                                   </thead>
                                   <tbody>
                                     @foreach (($datas['Landscape']) as $row)
                                       <tr>
                                         <td width='30%'>
                                           <a class="fancybox img-fluid" href="{{asset($row->imageshowing)}}"
                                              data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                                               <img alt="gallery" src="{{asset($row->imageshowing)}}" class="all studio" style="width:100%"/>
                                           </a>
                                         </td>

                                         <td>
                                           {{
                                             str_limit(strip_tags($row->text) , 200, '...')
                                           }}
                                         </td>
                                         <td>
                                           {{
                                             $row->imgpopup ? 'Popup' : 'Main'
                                           }}
                                         </td>
                                         <td>
                                           <div class="btn-group drop_btn" role="group">
                                               <button type="button" class="button button-pill button-royal-flat hvr-push "
                                                       id="exampleIconDropdown1" data-toggle="dropdown"
                                                       aria-expanded="false">
                                                   Action
                                                   <span class="caret"></span>
                                               </button>
                                               <ul class="dropdown-menu dropdown_position1" aria-labelledby="exampleIconDropdown1"
                                                   role="menu">
                                                   <li role="presentation">
                                                     <a href="{{url('AOL/'.$row->id.'/edit')}}" role="menuitem">Edit</a></li>
                                                   <li role="presentation">
                                                     <form action="{{url('AOL/'.$row->id)}}" method="post" name='form{{$row->id}}'>
                                                       {{method_field('DELETE')}}
                                                       {{ csrf_field() }}
                                                     </form>
                                                     <a href="javascript:void(0)" onclick="document.forms['form{{$row->id}}'].submit(); return false;"
                                                                              role="menuitem">Delete</a>
                                                     </li>
                                               </ul>
                                           </div>
                                         </td>
                                       </tr>
                                     @endforeach
                                   </tbody>
                                 </table>

                               </div>
                               <div class="tab-pane fade" id="Interior">
                                 <div class="col-lg-12 text-right">
                                   <a href="{{url('AOL/Interior')}}" class="button button-pill button-action-flat hvr-pulse">
                                     เพิ่มข้อมูล Interior
                                   </a>
                                 </div>
                               </br>
                                 <table class="table table-striped table-bordered table-hover datatable" >
                                   <thead>
                                     <th class="text-center">รูป</th>
                                     <th class="text-center">ข้อความ</th>
                                     <th class="text-center">Type</th>
                                     <th class="text-center"></th>
                                   </thead>
                                   <tbody>
                                     @foreach (($datas['Interior']) as $row)
                                       <tr>
                                         <td width='30%'>
                                           <a class="fancybox img-fluid" href="{{asset($row->imageshowing)}}"
                                              data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                                               <img alt="gallery" src="{{asset($row->imageshowing)}}" class="all studio" style="width:100%"/>
                                           </a>
                                         </td>

                                         <td>
                                           {{
                                             str_limit(strip_tags($row->text) , 200, '...')
                                           }}
                                         </td>
                                         <td>
                                           {{
                                             $row->imgpopup ? 'Popup' : 'Main'
                                           }}
                                         </td>
                                         <td>
                                           <div class="btn-group drop_btn" role="group">
                                               <button type="button" class="button button-pill button-royal-flat hvr-push "
                                                       id="exampleIconDropdown1" data-toggle="dropdown"
                                                       aria-expanded="false">
                                                   Action
                                                   <span class="caret"></span>
                                               </button>
                                               <ul class="dropdown-menu dropdown_position1" aria-labelledby="exampleIconDropdown1"
                                                   role="menu">
                                                   <li role="presentation">
                                                     <a href="{{url('AOL/'.$row->id.'/edit')}}" role="menuitem">Edit</a></li>
                                                   <li role="presentation">
                                                     <form action="{{url('AOL/'.$row->id)}}" method="post" name='form{{$row->id}}'>
                                                       {{method_field('DELETE')}}
                                                       {{ csrf_field() }}
                                                     </form>
                                                     <a href="javascript:void(0)" onclick="document.forms['form{{$row->id}}'].submit(); return false;"
                                                                              role="menuitem">Delete</a>
                                                     </li>
                                               </ul>
                                           </div>
                                         </td>
                                       </tr>
                                     @endforeach
                                   </tbody>
                                 </table>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

         </div>
         <!-- First Basic Table Ends Here-->
         <!-- Second Data Table Starts Here-->

         <!--rightside bar -->

      </section>
      <!-- /.content -->
   </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="vendors/datatables/js/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="js/custom_js/datatables_custom.js"></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script src="js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="vendors/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="vendors/fancybox/helpers/jquery.fancybox-buttons.js" type="text/javascript"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="vendors/fancybox/jquery.fancybox.js"></script>
    <script src="js/animated-masonry-gallery.js" type="text/javascript"></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script type="text/javascript" src="js/buttons.js"></script>
    <script type="text/javascript" src="vendors/laddabootstrap/js/spin.min.js"></script>
    <script type="text/javascript" src="vendors/laddabootstrap/js/ladda.min.js"></script>
    <script type="text/javascript" src="vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script type="text/javascript" src="js/custom_js/button_main.js"></script>
    <!-- end of page level js -->
@stop

@extends('layouts.default')
@section('title')
    Dropify | Clear Admin Template
@stop
@section('styles')
    <!--page level css -->
    <link rel="stylesheet" href="{{asset('css/blueimp-gallery.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('vendors/dropify/css/dropify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom_css/dropify.css')}}">
    <!--end of page level css-->


    <!--page level css -->
    <link href="{{asset('vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('vendors/trumbowyg/ui/trumbowyg.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="screen" type="text/css" href="{{asset('css/summernote.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/form_editors.css')}}">
    <!--end of page level css-->



    <!--page level css -->
    <link href="{{asset('')}}/css/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
    <!-- labelauty -->
    <link href="{{asset('')}}/vendors/jquerylabel/css/jquery-labelauty.css" rel="stylesheet" type="text/css"/>
    <!--select css-->
    <link href="{{asset('')}}/vendors/select2/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('')}}/vendors/select2/css/select2-bootstrap.css"/>
    <!--clock face css-->
    <link href="{{asset('')}}/vendors/iCheck/css/all.css" rel="stylesheet"/>
    <link href="{{asset('')}}/vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('')}}/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}/css/custom_css/radio_checkbox.css">
@stop
@section('content')
    <!-- /.right-side -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                เพิ่มข้อมูล {{$grouptype}}
            </h1>

        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-fw ti-dropbox"></i> {{$grouptype}}
                            </h3>
                        </div>
                        <div class="card-body p-30">

                            <form class="" action="{{url('AOL')}}" method="post" enctype="multipart/form-data">
                               {{method_field('POST')}}
                               {{ csrf_field() }}


                               <input type="text" name="type" value="{{$grouptype}}" readonly hidden />

                              {{-- Row --}}
                              <div class="row">
                                  <div class="col-lg-12">
                                    <div class="form-group">

                                      {{-- Radio check --}}
                                      <label for="">ประเภทการแสดง</label>
                                      <div class="row">
                                          <div class="col-md-2">
                                              <div class="radio radio-info radio-inline m-l-18">
                                                  <input type="radio" id="inlineRadio1" value="main"
                                                         name="radio" checked>
                                                  <label for="inlineRadio1"> &nbsp;หลัก </label>
                                              </div>
                                          </div>
                                          <div class="col-md-2">
                                              <div class="radio radio-inline m-l-18">
                                                  <input type="radio" id="inlineRadio2" value="below"
                                                         name="radio">
                                                  <label for="inlineRadio2"> &nbsp;Popup </label>
                                              </div>
                                          </div>
                                      </div>
                                      {{-- End Radio Check --}}


                                    </div>
                                  </div>

                                  {{-- Main image --}}
                                  <div class="col-md-6">
                                      <h5 class="h5pnl_font">Upload Main (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" required name='img' data-default-file='{{asset('/backend/storage/app/logouploadimage/1200x576.png')}}'/>
                                  </div>
                                  {{-- End main  --}}

                                  {{-- Popup --}}
                                  <div class="col-md-6" id='popup'>
                                      <h5 class="h5pnl_font">Upload Popup (File Type: PNG, JPG )</h5>
                                      <input type="file" class="dropify" data-allowed-file-extensions="png jpg" name='imgpopup'/>
                                  </div>
                                  {{-- End popup --}}


                                  <div class="col-md-12">
                                      <div class="row">

                                        {{-- Text --}}
                                        <div class="col-md-12">
                                          <div class="form-group" style="margin-top:1%">
                                            <label for="">ข้อความ</label>
                                            <textarea name="text" id='summernote'></textarea>
                                          </div>
                                        </div>
                                        {{-- End Tetx --}}

                                        {{-- BTN Submit --}}
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary m-t-10">
                                            Submit
                                          </button>
                                          <a href="{{url('AOL')}}" class="btn btn-danger m-t-10">Cancel</a>
                                        </div>


                                      </div>
                                  </div>
                              </div>
                              {{-- End row --}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--rightside bar -->

            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
@stop
@section('scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('vendors/dropify/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/dropify_custom.js')}}" ></script>
    <!-- end of page level js -->


    <!-- begining of page level js -->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/trumbowyg/js/trumbowyg.js')}}" type="text/javascript"></script>
    <!--<script src="vendors/trumbowyg/ui/icons.svg" type="text/javascript"></script>-->
    <script src="{{asset('vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom_js/form_editors.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->

    <script>
    $("#popup").hide()
    $('input[type=radio][name=radio]').change(function() {
      if (this.value == 'main') {
        $("#popup").hide()
      } else {
        $("#popup").show()
      }
    });
    </script>
@stop

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\place_serviceandworkModel as service;

class serviceandworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('serviceandwork.index', ['resources' => service::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('serviceandwork.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->_insertupdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('serviceandwork.edit', ['resources' => service::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_insertupdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = service::find($id);
        \lib::remove($service->pathfile);
        $service->delete();
        return redirect('serviceandwork');
    }

    public function _insertupdate ($r, $id = '') {
      $service = new service();

      if ($id != '') {
        $service = service::find($id);
      }

      if ($r->hasFile('img')) {
        \lib::remove($service->pathfile);
        $service->pathfile = \lib::upload($r->img, 570, 642);
      }

      $service->range = $r->range;
      $service->save();
      return redirect('serviceandwork');
    }
}

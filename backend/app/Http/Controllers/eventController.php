<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\eventsModel as event;
use App\Models\eventimgslitesModel as eimg;

class eventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('event.index', ['resources' => event::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {
      $event = new event();
      $event->title = $r->texttitle;
      $event->detail = $r->detail;
      $event->eventDate = $r->eventdate;
      $event->eventTime = $r->eventtime;
      $event->eventLocation = $r->eventlocation;


      if ($r->hasFile('img')) {
        $event->imgtitle = \lib::upload($r->img, 250, 250);
      }
      $event->save();

      if ($r->hasFile('imgother')) {
        foreach ($r->imgother as $img ) {
          $filename = \lib::upload($img, 476, 317);
          $eimg = new eimg();
          $eimg->img = $filename;
          $eimg->e_id = $event->id;
          $eimg->save();
        }
      }

      return redirect('event');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('event.edit', ['resources' => event::with('eventimgslites')->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $e = event::find($id);
      $e->title = $r->texttitle;
      $e->detail = $r->detail;
      $e->eventDate = $r->eventdate;
      $e->eventTime = $r->eventtime;
      $e->eventLocation = $r->eventlocation;

    if ($r->hasFile('img')) {
      // ลบรูปเก่า
      \lib::remove($e->imgtitle);
      $filename = \lib::upload($r->img, 370, 246);
      $e->imgtitle = $filename;
    }

    $e->save();

    if ($r->hasFile('imgother')) {
      foreach ($r->imgother as $img ) {
        $filename = \lib::upload($img, 476, 556);
        $eimg = new eimg();
        $eimg->img = $filename;
        $eimg->e_id = $e->id;
        $eimg->save();
      }
    }

    return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e =  event::with('eventimgslites')->find($id);
        \lib::remove($e->imgtitle);

        foreach ($e->eventimgslites as $row) {
          \lib::remove($row->img);
          $row->delete();
        }
        $e->delete();
        return back();
    }


    public function eventlistremove ($id) {
      $eimg = eimg::find($id);
      \lib::remove($eimg->img);
      $eimg->delete();
      return back();
    }
}

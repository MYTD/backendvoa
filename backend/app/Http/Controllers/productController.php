<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\productsModel as product;
class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index', ['resources' => product::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->_insertorupdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('products.edit', ['resources' => product::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_insertorupdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = product::find($id);
        \lib::remove($product->img);
        $product->delete();
        return redirect('product');

    }

    public function _insertorupdate ($r, $id = '') {
      $product = new product();
      if ($id != '') {
        $product = product::find($id);
      }

      if ($r->hasFile('img')) {
        if ($id != '') {
          \lib::remove($product->img);
        }
        $filename = \lib::upload($r->img, $r->w, $r->h);
        $product->img  = $filename;

      }
      $product->w = $r->w;
      $product->h = $r->h;
      $product->link = $r->url;
      $product->type	 = $r->type;
      $product->range = $r->range;
      $product->save();

      return redirect('product');
    }
}

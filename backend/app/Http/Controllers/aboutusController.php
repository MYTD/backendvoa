<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\aboutusModel as aboutus;
class aboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('aboutus.index', ['resources' => aboutus::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aboutus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->_insertupdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('aboutus.edit', ['resources' => aboutus::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_insertupdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aboutus = aboutus::find($id);
        \lib::remove($aboutus->img);
        $aboutus->delete();
        return redirect('aboutus');

    }

    public function _insertupdate ($r, $id = '') {
      $aboutus = new aboutus();

      if ($id != '') {
        $aboutus = aboutus::find($id);
      }

      if ($r->hasFile('img')) {
        \lib::remove($aboutus->img);
        $aboutus->img = \lib::upload($r->img, 800, null);
      }
      $aboutus->text = $r->text;
      $aboutus->save();

      return redirect('aboutus');
    }
}

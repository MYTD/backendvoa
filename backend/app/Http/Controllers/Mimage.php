<?php

namespace App\Http\Controllers;
class Mimage
{
  public static function destroyIMG($filename) {
    $img = str_replace('/backend/storage/', '', $filename);
    if ($img == '') return '';
    if (file_exists(storage_path($img))) {
      unlink(storage_path($img));
    }
  }

  public static function addIMG ($image, $w = null, $h = null ) {
    ini_set('memory_limit','2156M');
    // try {
      //resize image
      // $imgwidth = $width;
      //folder upload (public/imgpics)
      $folderupload = 'app/images';

      $file = $image;
      //
      $filename = time() . $file->getClientOriginalName();
      // $path = public_path($folderupload.'/' . $filename);
      $path = storage_path($folderupload.'/' . $filename);
      //
      // create instance of Intervention Image
      $img = \Image::make($file->getRealPath());
      // if($img->width()>$imgwidth) {
      // 	// See the docs - http://image.intervention.io/api/resize
      // 	// resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img->resize($w, $h, function ($constraint) {
          $constraint->aspectRatio();
        });
      // }

      // Save Image
      $img->save($path);

      $filename = '/backend/storage/app/images/'.$filename;
      return $filename;
    // } catch (\Exception $e) {
    //   return $e;
    // }
  }
}

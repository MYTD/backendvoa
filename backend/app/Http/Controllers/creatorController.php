<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\creatorModel as creator;
use App\Models\creatorlistjobModel as creatorjob;

class creatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('creator.index', ['resources' => creator::with('creatorjob')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('creator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      if ($r->hasFile('img')) {

        $filename = \lib::upload($r->img, 370, 680);
        $creator = new creator();
        $creator->image = $filename;
        $creator->save();
      }

      if ($r->hasFile('imagejob')) {
        $i = 0 ;

        foreach ($r->imagejob as $img) {

          $r->text[$i++];
          $filename = \lib::upload($img, 970, 650);
          $creatorJob = new creatorjob();
          $creatorJob->listimg = $filename;
          $creatorJob->creator_id = $creator->id;
          $creatorJob->save();
        }
      }
      return redirect('creator');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('creator.edit', ['resources' => creator::with('creatorjob')->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {

        if ($r->hasFile('img')) {
          $creator = creator::find($id);
          \lib::remove($creator->image);
          $filename = \lib::upload($r->img, 370, 680);
          $creator->image = $filename;
          $creator->save();
        }

      if ($r->hasFile('imagejob')) {
        foreach ($r->imagejob as $img) {
          $filename = \lib::upload($img, 970, 650);
          $creatorJob = new creatorjob();
          $creatorJob->listimg = $filename;
          $creatorJob->creator_id = $id;
          $creatorJob->save();
        }
      }


      return redirect('creator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = creator::with('creatorjob')->find($id);
        foreach ($data->creatorjob as $row) {
          \lib::remove($row->listimg);
          $row->delete();
        }
        \lib::remove($data->image);
        $data->delete();
        return redirect('creator');
    }

    public function imagejobdelete ($id) {
      $creatorjob = creatorjob::find($id);
      \lib::remove($creatorjob->listimg);
      $creatorjob->delete();
      return back();
    }


}

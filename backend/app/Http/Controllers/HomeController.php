<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\indeximgModel as IIMG;
use App\Models\indextextModel as ITM;
// use App\Http\Controllers\Mimage;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('homes.index', ['resources' => IIMG::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return $this->_inserupdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('homes.edit', ['resources' => IIMG::find($id) ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_inserupdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $IIMG = IIMG::find($id);

        \lib::remove($IIMG->nameimg);
        $IIMG->delete();
        return back();
    }

    public function _inserupdate($r, $id = '') {
      $IIMG = new IIMG();

      // if update / find id
      if ($id != '') {
        $IIMG = IIMG::find($id);
      }

      // Check File
      if ($r->hasFile('img')) {
        if ($IIMG->nameimg) \lib::remove($IIMG->nameimg);

        $IIMG->nameimg = \lib::upload($r->img, 1920, 842);
      }
      $IIMG->range =  $r->range;

      $IIMG->save();
      return redirect('home');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\placeaestheticsoflivingModel as aol;

class AOLController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = [
          'Exterior'  => $this->getResources('Exterior'),
          'Landscape' => $this->getResources('Landscape'),
          'Interior'  => $this->getResources('Interior')
        ];

        return view('aestheticsOfLiving.index',[
          'datas' => $datas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        return $this->_insertorupdate($r);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('aestheticsOfLiving.create',['grouptype' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('aestheticsOfLiving.edit', ['resources' => aol::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_insertorupdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aol = aol::find($id);
        \lib::remove($aol->imageshowing);
        \lib::remove($aol->imgpopup);
        $aol->delete();
        return back();
    }

    public function getResources($type) {
      return aol::where('type', $type)->orderBy('imgpopup')->get();
      // return [
      //   'mains' => aol::where('type', $type)->whereNull('imgpopup')->get(),
      //   'popups' => aol::where('type', $type)->whereNotNull('imgpopup')->get()
      // ];
    }


    public function _insertorupdate($r, $id = '') {
      $aol = new aol();
      if ($id != '') $aol = aol::find($id);

      /**
       * ถ้าเป็นการ upload รูปหลัก จะเข้า if
       */
      if ($r->radio == 'main') {
        /**
         * ตรวจสอบ ว่ามีไฟล์ หรือไม่
         * กรณี Upload อาจจะมีไฟล์ หรือไม่มีก็ได้
         */
        if ($r->hasFile('img')) {
            /**
             * ถ้ามีการแก้ไขรูปจะ ลบรูปเก่าออกก่อน
             */
            if ($aol->imageshowing) \lib::remove($aol->imageshowing);
            $aol->imageshowing = \lib::upload($r->img, 1200, 576);
        }
      }

      if ($r->radio == 'below') {

        if ($r->hasFile('img')) {
            if ($aol->img) \lib::remove($aol->imageshowing);
            $aol->imageshowing = \lib::upload($r->img , 350, 350);
        }

        if ($r->hasFile('imgpopup')) {
          if ($aol->imgpopup) \lib::remove($aol->imgpopup);
          $aol->imgpopup = \lib::upload($r->imgpopup , 1200, 576);
        }


      }

      $aol->text = $r->text;
      $aol->type = $r->type;
      $aol->save();

      return redirect('AOL');
    }
}

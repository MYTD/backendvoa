<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\eventsModel as event;
class eventsAPI extends Controller
{
  public function index () {
      return response()->json(['resource' => event::with('eventimgslites')->get()]);
    }

    public function readmore($id) {
      try {
        $recap = addslashes(strip_tags(trim($id)));
        return response()->json(['resource' => event::with('eventimgslites')->find($recap)]);
      } catch (\Exception $e) {
        return response()->json('error', 422);
      }
    }
}

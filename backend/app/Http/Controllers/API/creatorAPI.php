<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\creatorModel as creator;

class creatorAPI extends Controller
{
    public function index () {
      return response()->json(['resource' => creator::with('creatorjob')->get()]);
    }
}

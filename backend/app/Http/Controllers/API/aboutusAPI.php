<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\aboutusModel as aboutus;

class aboutusAPI extends Controller
{
    public function index () {
      return response()->json(['resource' => aboutus::get()]);
    }
}

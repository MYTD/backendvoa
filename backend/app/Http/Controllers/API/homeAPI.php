<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\indeximgModel as IIMG;
use App\Models\indextextModel as ITM;

class homeAPI extends Controller
{
    public function index () {
      $resource['iimg'] = IIMG::select('nameimg')->orderBy('range')->get();
      $resource['itm'] = ITM::first();
      return response()->json($resource, 200);
    }
}

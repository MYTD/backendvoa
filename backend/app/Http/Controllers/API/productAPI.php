<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\productsModel as product;

class productAPI extends Controller
{
    public function index () {
      return response()->json(['resource' => product::orderBy('type')->orderBy('range')->get()]);
    }
}

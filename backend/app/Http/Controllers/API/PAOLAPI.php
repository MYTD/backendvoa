<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\placeaestheticsoflivingModel as paol;
class PAOLAPI extends Controller
{
    public function index () {
      $resource = paol::orderBy('type')->orderBy('imgpopup')->get();
      return response()->json(['resource' => $resource]);
    }
}

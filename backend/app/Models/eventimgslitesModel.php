<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class eventimgslitesModel extends Model
{

  protected $table = 'eventimgslites';
  public $primaryKey = 'id';
  public $timestamps = true;

  protected $hidden = ['updated_at', 'created_at', 'id', 'e_id'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class indeximgModel extends Model
{
    protected $table = 'indeximgslites';
    public $primaryKey = 'id';
    public $timestamps = true;
}

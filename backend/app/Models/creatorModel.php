<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class creatorModel extends Model
{
  protected $table = 'creators';
  public $primaryKey = 'id';
  public $timestamps = true;

  public function creatorjob()
  {
    return $this->hasMany('App\Models\creatorlistjobModel', 'creator_id', 'id');
  }
}

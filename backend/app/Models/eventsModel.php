<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class eventsModel extends Model
{
  protected $table = 'events';
  public $primaryKey = 'id';
  public $timestamps = true;

  protected $hidden = ['updated_at', 'created_at'];

  public function eventimgslites()
  {
    return $this->hasMany('App\Models\eventimgslitesModel', 'e_id', 'id');
  }
}

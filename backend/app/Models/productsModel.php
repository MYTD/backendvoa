<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class productsModel extends Model
{
  protected $table = 'products';
  public $primaryKey = 'id';
  public $timestamps = false;


  protected $hidden = ['id'];
}
// $table->text('img');
// $table->text('link')->nullable();

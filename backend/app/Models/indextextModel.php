<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class indextextModel extends Model
{
  protected $table = 'indextext';
  public $primaryKey = 'id';
  public $timestamps = true;
}

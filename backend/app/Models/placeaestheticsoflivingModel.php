<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class placeaestheticsoflivingModel extends Model
{
    //
    protected $table = 'placeaestheticsofliving';
    public $primaryKey = 'id';
    public $timestamps = true;
}

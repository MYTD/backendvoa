<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class creatorlistjobModel extends Model
{
  protected $table = 'creatorlistjob';
  public $primaryKey = 'id';
  public $timestamps = false;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class place_serviceandworkModel extends Model
{
  protected $table = 'place_serviceandwork';
  public $primaryKey = 'id';
  public $timestamps = false;


  protected $hidden = ['id'];
}

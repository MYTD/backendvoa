<?php

use Illuminate\Http\Request;

Route::get('index', 'API\homeAPI@index');
Route::get('events', 'API\eventsAPI@index');
Route::get('events/readmore/{id}', 'API\eventsAPI@readmore');
Route::get('aboutus', 'API\aboutusAPI@index');
Route::get('paol', 'API\PAOLAPI@index');
Route::get('serviceandwork', 'API\serviceandwordAPI@index');
Route::get('creator', 'API\creatorAPI@index');
Route::get('product', 'API\productAPI@index');

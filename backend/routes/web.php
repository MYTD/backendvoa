<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('login');
});
// Route::get('{name?}', 'ClearDarkController@showView');


Auth::routes();


Route::middleware(['auth'])->group(function () {
  Route::resources([
    'home' => 'HomeController',
    'hometext' => 'homeTextController',
    'aboutus' => 'aboutusController',
    'serviceandwork' => 'serviceandworkController',
    'product' => 'productController',
    'creator' => 'creatorController',
    'event' => 'eventController',
    'AOL' => 'AOLController',
    'user' => 'userController',
  ]);

  Route::DELETE('creator/imagejob/{id}', 'creatorController@imagejobdelete');
  Route::DELETE('event/eventlist/{id}', 'eventController@eventlistremove');

});
